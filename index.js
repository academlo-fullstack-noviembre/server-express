import express from "express";
import path from "path";
const app = express();

// | peticion (request) | middleware (servidor) | respuesta (response)

//app.use para manejar cualquier tipo de petición con los métodos GET, POST, DELETE, PUT, PATCH
//express.urlencoded para procesar los datos que me esté enviado el cliente con el formato (urlencode) => 
//al procesarlos los pondrá en el objeto request sobre la propiedad body (request.body)
app.use(express.urlencoded());

//middleware
app.get("/merch", (request, response) => {
    response.send("<h1>hello world</h1>")
});

app.get("/playeras", (request, response) => {
    response.send("Bienvenido a mi tienda en línea");
});

app.get("/contacto", (request, response) => {
    //Para construir la ruta absoluta hacía un archivo
    const contactoFile = path.join(__dirname, "contacto.html");
    //Responder con un archivo
    response.sendFile(contactoFile);
});

app.post("/usuarios", (request, response) => {
    // let name = request.body.name;
    // let lastname = request.body.lastname;
    //Desestructurando
    let {name, lastname} = request.body;
    console.log(name, lastname);
    response.status(201).send("Se ha guardado el usuario en el sistema");
});

app.listen(8000, () => {
    console.log("Servidor escuchando sobre el puerto 8000");
});